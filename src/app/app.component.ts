import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  postsArrayInit = [
    {
      title: 'Post1',
      content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
      loveIts: 0,
      createdAt: new Date()
    },
    {
      title: "Post2",
      content: "Lorem ipsum...",
      loveIts: -1,
      createdAt: new Date()
    },
    {
      title: "Post3",
      content: "Un dernier paragraphe plein de pertinence",
      loveIts: 1,
      createdAt: new Date()
    }
  ];
}
